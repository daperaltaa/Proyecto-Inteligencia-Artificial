import numpy as np
import os
import re
import matplotlib.pyplot as plt
from tensorflow.keras import models
from skimage import data
from skimage.transform import resize
from tkinter import *
from tkinter import Image
import tkinter.font as tkFont
from PIL import Image, ImageTk

app = Tk()

# Definimos el directorio de la ubicacion del dataset
dirname = os.path.join(os.getcwd(), 'dataset')
imgpath = dirname + os.sep 

def prediccion():

    #Definimos vectores necesarios
    images = []
    directories = []
    dircount = []
    prevRoot=''
    cant=0

    #Se recorre en el dataset cada una de las carpetas que contienen las imagenes para el entrenamiento
    for root, dirnames, filenames in os.walk(imgpath):
        for filename in filenames:
            #Se buscan las imagenes segun su extencion y se hace el conteo de cuantas imagenes son por carpeta
            if re.search("\.(jpg|jpeg|png|bmp|tiff)$", filename):
                cant=cant+1
                filepath = os.path.join(root, filename)
                #Se leen las imagenes
                image = plt.imread(filepath)
                #Si es necesario se redimencionan (21px*28px) y se les agrega un anti aliasing para facilitar el entrenamiento
                image_resized = resize(image, (21, 28),anti_aliasing=True,clip=False,preserve_range=True)
                images.append(image_resized)
                b = "Leyendo..." + str(cant)
                if prevRoot !=root:
                    prevRoot=root
                    directories.append(root)
                    dircount.append(cant)
                    cant=0
                    
    #Conteo de directorios e imagenes
    dircount.append(cant)
    
    dircount = dircount[1:]
    dircount[0]=dircount[0]+1

    #Guardamos labels con las etiquetas creadas de cada uno de los directorios para las salidas 
    labels=[]
    indice=0
    for cantidad in dircount:
        for i in range(cantidad):
            labels.append(indice)
        indice=indice+1

    #Se extraen los nombre de cada uno de los directorios, "esta funcion sera utilizada para la prediccion"
    letras=[]
    indice=0
    for directorio in directories:
        name = directorio.split(os.sep)
        letras.append(name[len(name)-1])
        indice=indice+1

    imagenes=[]

    # Cargamos el modelo entrenado
    model = models.load_model("LSEÑAS2.h5")

    #Guardamos la direccion de la imagen que nos arrojo la funcion de abrir
    dirimg = abrir()
    #Guardamos esa direccion en un vector
    filenames = [dirimg]

    #Seleccioamos esa imagen para redimencionarla
    for filepath in filenames:
        image = plt.imread(filepath,0)
        image_resized = resize(image, (21, 28),anti_aliasing=True,clip=False,preserve_range=True)
        imagenes.append(image_resized) 

    # Convertimos imagen a vectores
    X = np.array(imagenes, dtype=np.uint8) # Convertir de lista a array numpy
    test_X = X.astype('float32')
    test_X = test_X / 255.

    # Realizamos la prediccion de los valores que tenemos de la imagen con los de la red neuronal
    predicted_classes = model.predict(test_X) 
    fontStyle = tkFont.Font(family="Lucida Grande", size=100)

    # Se hace el proceso para mostrar la prediccion de la letra
    for i, img_tagged in enumerate(predicted_classes):

        #Se guarda el resultado de la prediccion segun las salidas que se definieron en la clase entrenamiento donde estan los nombres
        #de los directorios, A, B, C...
        letra = letras[img_tagged.tolist().index(max(img_tagged))]
        letras = Label(app, text = "La letra es: ")
        letras.place(relx=0.7,rely=0.1)  
        borrar = Label(app, text="   ", font=fontStyle)
        borrar.place(relx=0.7,rely=0.2)
        # Se muestra en un label
        pre_letra = Label(app, text = letra, font=fontStyle)
        pre_letra.place(relx=0.7,rely=0.2)

def abrir():
    ruta_archivo = filedialog.askopenfilename(initialdir = "./test/",
                                        title = "Seleccione una foto", filetypes = (("jpeg files","*.jpg"),
                                        ("all files","*.*")))

    nom_archivo = os.path.basename(ruta_archivo)
    archivo = "test/"+nom_archivo

    # Se imprime en el Tkinter la imagen que ha sido seleccionada para comparar con los resultados de la red 
    img = Image.open(archivo)
    n_img = img.resize((388,256))
    render = ImageTk.PhotoImage(n_img)
    img1 = Label(app, image = render)
    img1.image = render
    img1.place(x=120, y=10)

    return archivo 

def main():

    # Se define el boton para predecir la imagen que se selecciono 
    predecir = Button(app, text = "Predecir",width=10,height=4, command=prediccion).place(x=10, y=10)

if __name__ == '__main__':

    main()
    app.geometry("800x300")
    app.title("Reconocimiento Lenguaje de Señas")
    app.config(bg="#7fb0dc")
    app.mainloop()