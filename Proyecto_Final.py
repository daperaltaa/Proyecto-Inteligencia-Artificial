import numpy as np
import os
import re
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import keras
from keras.utils import to_categorical
from keras.models import Sequential,Input,Model
from tensorflow.keras import models
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from skimage import data
from skimage.transform import resize
from PIL import Image, ImageTk

# Definimos el directorio de la ubicacion del dataset
dirname = os.path.join(os.getcwd(), 'dataset')
imgpath = dirname + os.sep 

#Iniciamos clase de entrenamiento porque despues de entrenar necesitaremos algunos datos para la prediccion
def entrenamiento():

    #Definimos vectores necesarios
    images = []
    directories = []
    dircount = []
    prevRoot=''
    cant=0

    #Se recorre en el dataset cada una de las carpetas que contienen las imagenes para el entrenamiento
    for root, dirnames, filenames in os.walk(imgpath):
        for filename in filenames:
            #Se buscan las imagenes segun su extencion y se hace el conteo de cuantas imagenes son por carpeta
            if re.search("\.(jpg|jpeg|png|bmp|tiff)$", filename):
                cant=cant+1
                filepath = os.path.join(root, filename)
                #Se leen las imagenes
                image = plt.imread(filepath)
                #Si es necesario se redimencionan (21px*28px) y se les agrega un anti aliasing para facilitar el entrenamiento
                image_resized = resize(image, (21, 28),anti_aliasing=True,clip=False,preserve_range=True)
                images.append(image_resized)
                b = "Leyendo..." + str(cant)
                print (b, end="\r")
                if prevRoot !=root:
                    print(root, cant)
                    prevRoot=root
                    directories.append(root)
                    dircount.append(cant)
                    cant=0
                    
    #Conteo de directorios e imagenes
    dircount.append(cant)

    dircount = dircount[1:]
    dircount[0]=dircount[0]+1
    print('Directorios leidos:',len(directories))
    print("Imagenes en cada directorio", dircount)

    #Guardamos labels con las etiquetas creadas de cada uno de los directorios para las salidas 
    labels=[]
    indice=0
    for cantidad in dircount:
        for i in range(cantidad):
            labels.append(indice)
        indice=indice+1
    print("Cantidad etiquetas creadas: ",len(labels))

    #Se extraen los nombre de cada uno de los directorios, "esta funcion sera utilizada para la prediccion"
    letras=[]
    indice=0
    for directorio in directories:
        name = directorio.split(os.sep)
        print(indice , name[len(name)-1])
        letras.append(name[len(name)-1])
        indice=indice+1

    y = np.array(labels)
    X = np.array(images, dtype=np.uint8) #conversion de las listas a array con numpy

    # Proceso para preparar datos, entrenar el modelo y mostrar metricas

    # Encontrar los numeros unicos de cada etiqueta para el entrenamiento
    classes = np.unique(y)
    nClasses = len(classes)
    print('Numero total de salidas : ', nClasses)
    print('Numero total de clases : ', classes)

    #Definicion del train y test tanto de x como de y
    train_X,test_X,train_Y,test_Y = train_test_split(X,y,test_size=0.2)

    plt.figure(figsize=[5,5])

    # Se muestra la primera imagen en los datos de entrenamiento
    plt.subplot(121)
    plt.imshow(train_X[0,:,:], cmap='gray')
    plt.title("presicion : {}".format(train_Y[0]))

    # Se muestra la primera imagen en los datos de test
    plt.subplot(122)
    plt.imshow(test_X[0,:,:], cmap='gray')
    plt.title("Presicion : {}".format(test_Y[0]))


    train_X = train_X.astype('float32')
    test_X = test_X.astype('float32')
    train_X = train_X / 255.
    test_X = test_X / 255.

    # Cambiar las etiquetas de categoria a one-hot para entrenar la red
    train_Y_one_hot = to_categorical(train_Y)
    test_Y_one_hot = to_categorical(test_Y)

    
    #Modelo
    #Crear los grupos de entrenamiento y testing
    train_X,valid_X,train_label,valid_label = train_test_split(train_X, train_Y_one_hot, test_size=0.2, random_state=13)

    #Declaramos cada una de las variables con los parámetros de configuración de la red
    INIT_LR = 1e-3 # Valor inicial de learning rate.
    epochs = 2000 # Cantidad de epocas
    batch_size = 64 # cantidad de imágenes que se toman a la vez

    #Creacion del modelo con cada una de sus convoluciones
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),activation='linear',padding='same',input_shape=(21,28,3)))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling2D((2, 2),padding='same'))
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(32, activation='linear'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(Dropout(0.5))
    model.add(Dense(nClasses, activation='softmax'))

    model.summary()

    model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adagrad(lr=INIT_LR, decay=INIT_LR / 100),metrics=['accuracy'])

    # Entrenamiento del modelo    
    model_train = model.fit(train_X, train_label, batch_size=batch_size,epochs=epochs,verbose=1,validation_data=(valid_X, valid_label))

    # Se guarda el modelo de la red neuronal
    model.save("LSEÑAS3.h5")

    test_eval = model.evaluate(test_X, test_Y_one_hot, verbose=1)

    #Se imprime los datos de presicion del entrenamiento
    print('Test loss:', test_eval[0])
    print('Test accuracy:', test_eval[1])

    #Se muestra la grafica de presicion del entrenamiento y validacion
    accuracy = model_train.history['acc']
    val_accuracy = model_train.history['val_acc']
    loss = model_train.history['loss']
    val_loss = model_train.history['val_loss']
    epochs = range(len(accuracy))
    plt.plot(epochs, accuracy, 'bo', label='Training accuracy')
    plt.plot(epochs, val_accuracy, 'b', label='Validation accuracy')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.figure()
    plt.plot(epochs, loss, 'bo', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    plt.show()   

if __name__ == '__main__':

    entrenamiento()